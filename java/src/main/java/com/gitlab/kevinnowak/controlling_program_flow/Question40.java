package com.gitlab.kevinnowak.controlling_program_flow;

import java.util.List;

public class Question40 {

    public static void main(String[] args) {
        final var javaVersion = List.of(17, 11, 8);
        var names = List.of("JDK", "Java");
        V: for (var e1 : javaVersion) {
            E: for (String e2 : names)
                System.out.println(e1 + "_" + e2);
                break;
        }
    }
}
