package com.gitlab.kevinnowak.controlling_program_flow;

public class Penguins {

    public static void main(String[] args) {
        var pen = new Penguins();
        pen.length("penguins");
        pen.length(5);
        pen.length(new Object());
    }

    public void length(Object obj) {
        if (obj instanceof String) {
            System.out.println((((String) obj).length()));
        }
    }
}
