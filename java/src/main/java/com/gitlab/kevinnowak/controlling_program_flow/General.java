package com.gitlab.kevinnowak.controlling_program_flow;

enum Season {
    SPRING,
    SUMMER,
    WINTER
}


public class General {

    public static void main(String[] args) {

    }

    public int getAverageTemperate(Season s) {
        switch (s) {
            default:
            case WINTER, SPRING: return 30;
        }
    }
}
