package com.gitlab.kevinnowak.handling_exceptions;

public class Computer {

    public void computer() throws Exception {
        throw new NullPointerException("Does not compute!");
    }

    public static void main(String[] args) throws Exception {
        try {
            new Computer().computer();
        } catch (NullPointerException e) {
            System.out.println("zero");
            throw e;
        } catch (Exception e) {
            System.out.println("one");
            throw e;
        }
    }
}
