package com.gitlab.kevinnowak.streams;

import java.util.Optional;

public class OptionalExample {

    public static Optional<Double> average(int... scores) {
        if (scores.length == 0) {
            return Optional.empty();
        }

        int sum = 0;

        for (int score : scores) {
            sum += score;
        }

        return Optional.of((double) sum / scores.length);
    }

    public static void main(String[] args) {
        Optional<Double> opt1 = average(90, 100);
        opt1.ifPresent(System.out::println); // 95.0

        Optional<Double> opt2 = average();
        System.out.println(opt2.orElse(Double.NaN)); // Nan
        System.out.println(opt2.orElseGet(Math::random)); // Some random number

        Optional<Double> opt3 = average(90, 100);
        System.out.println(opt3.orElse(Double.NaN)); // 95.0
        System.out.println(opt3.orElseGet(Math::random)); // 95.0
        System.out.println(opt3.orElseThrow()); // 95.0

        Optional<Double> opt4 = average();
        System.out.println(opt4.orElseThrow(IllegalStateException::new)); // Throws exception
    }
}
