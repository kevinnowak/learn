package com.gitlab.kevinnowak.streams.creating_stream_sources.parallel_streams;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ParallelStreamExample {

    static long usingCollectionsParallel(Collection<Book> listOfBooks, int year) {
        AtomicLong countOfBooks = new AtomicLong();
        listOfBooks.parallelStream()
                .forEach(book -> {
                    if (book.getYearPublished() == year) {
                        countOfBooks.getAndIncrement();
                    }
                });

        System.out.print(listOfBooks.parallelStream().isParallel() + " ");

        return countOfBooks.get();
    }

    static long usingStreamParallel(Collection<Book> listOfBooks, int year) {
        AtomicLong countOfBooks = new AtomicLong();
        listOfBooks.stream().parallel()
                .forEach(book -> {
                    if (book.getYearPublished() == year) {
                        countOfBooks.getAndIncrement();
                    }
                });

        System.out.print(listOfBooks.stream().parallel().isParallel() + " ");

        return countOfBooks.get();
    }

    static long usingWithCustomSpliterator(MyBookContainer<Book> listOfBooks, int year) {
        AtomicLong countOfBooks = new AtomicLong();
        listOfBooks.parallelStream()
                .forEach(book -> {
                    if (book.getYearPublished() == year) {
                        countOfBooks.getAndIncrement();
                    }
                });

        System.out.print(listOfBooks.parallelStream().isParallel() + " "); // Here lays the betrayal

        return countOfBooks.get();
    }

    public static void main(String[] args) {
        var list = List.of(
                new Book("Book 1", "Author 1", 2020),
                new Book("Book 2", "Author 2", 2020),
                new Book("Book 3", "Author 3", 2018),
                new Book("Book 4", "Author 4", 2019),
                new Book("Book 5", "Author 1", 2020),
                new Book("Book 6", "Author 1", 2012),
                new Book("Book 7", "Author 5", 2011),
                new Book("Book 8", "Author 3", 2022),
                new Book("Book 9", "Author 6", 2023)
        );

        var bookContainer = new MyBookContainer<Book>(
                new Book[]{
                        new Book("Book 1", "Author 1", 2020),
                        new Book("Book 2", "Author 2", 2020),
                        new Book("Book 3", "Author 3", 2018),
                        new Book("Book 4", "Author 4", 2019),
                        new Book("Book 5", "Author 1", 2020),
                        new Book("Book 6", "Author 1", 2012),
                        new Book("Book 7", "Author 5", 2011),
                        new Book("Book 8", "Author 3", 2022),
                        new Book("Book 9", "Author 6", 2023)
                }
        );

        System.out.println(usingCollectionsParallel(list, 2020)); // true 3
        System.out.println(usingStreamParallel(list, 2020)); // true 3
        System.out.println(usingWithCustomSpliterator(bookContainer, 2020)); // false 0
    }
}
