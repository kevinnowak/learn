package com.gitlab.kevinnowak.streams.creating_stream_sources.parallel_streams;

import java.util.Spliterator;
import java.util.function.Consumer;

public class BookSpliterator<T> implements Spliterator<T> {
    private final Object[] books;
    private int startIndex;

    public BookSpliterator(Object[] books, int startIndex) {
        this.books = books;
        this.startIndex = startIndex;
    }

    @Override
    public Spliterator<T> trySplit() {
        return null;
    }

    @Override
    public boolean tryAdvance(Consumer<? super T> action) {
        return false;
    }

    @Override
    public long estimateSize() {
        return 0;
    }

    @Override
    public int characteristics() {
        return 0;
    }
}
